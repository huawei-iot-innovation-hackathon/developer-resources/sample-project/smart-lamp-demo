/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-executor.c
 */

#include "sld-executor.h"
#ifdef WITH_GPIOD
#include "sld-gpioctl.h"
#endif
#ifdef WITH_MQM
#include "sld-mqmproxy.h"
#endif

/**
 * @brief Post new event
 * @param executor A pointer to the executor object
 * @param type The type of the new event to be posted
 * @param payload The action payload
 */
static void
post_executor_event(SldExecutor *executor, ExecutorEventType type, ExecutorActionPayload payload);
/**
 * @brief GSource prepare function
 */
static gboolean executor_source_prepare(GSource *source, gint *timeout);
/**
 * @brief GSource dispatch function
 */
static gboolean executor_source_dispatch(GSource *source, GSourceFunc callback, gpointer _executor);
/**
 * @brief GSource callback function
 */
static gboolean executor_source_callback(gpointer _executor, gpointer _event);
/**
 * @brief GSource destroy notification callback function
 */
static void executor_source_destroy_notify(gpointer _executor);
/**
 * @brief Async queue destroy notification callback function
 */
static void executor_queue_destroy_notify(gpointer _executor);
/**
 * @brief Process lamp switch state from system bus
 */
static void do_update_mqm_lamp_state_event(SldExecutor *executor, const gchar *state);

/**
 * @brief GSourceFuncs vtable
 */
static GSourceFuncs executor_source_funcs = {
    executor_source_prepare,
    NULL,
    executor_source_dispatch,
    NULL,
    NULL,
    NULL,
};

static void
post_executor_event(SldExecutor *executor, ExecutorEventType type, ExecutorActionPayload payload)
{
    SldExecutorEvent *e = NULL;

    g_assert(executor);

    e = g_new0(SldExecutorEvent, 1);
    e->type = type;
    e->payload = payload;
    g_async_queue_push(executor->queue, e);
}

static gboolean executor_source_prepare(GSource *source, gint *timeout)
{
    SldExecutor *executor = (SldExecutor *) source;

    SLD_UNUSED(timeout);

    return (g_async_queue_length(executor->queue) > 0);
}

static gboolean executor_source_dispatch(GSource *source, GSourceFunc callback, gpointer _executor)
{
    SldExecutor *executor = (SldExecutor *) source;
    gpointer event = g_async_queue_try_pop(executor->queue);

    SLD_UNUSED(callback);
    SLD_UNUSED(_executor);

    if (event == NULL)
        return G_SOURCE_CONTINUE;

    return executor->callback(executor, event) == TRUE ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
}

static gboolean executor_source_callback(gpointer _executor, gpointer _event)
{
    SldExecutor *executor = (SldExecutor *) _executor;
    SldExecutorEvent *event = (SldExecutorEvent *) _event;

    g_assert(executor);
    g_assert(event);

    switch (event->type) {

    case EXECUTOR_UPDATE_MQM_LAMP_STATE:
        do_update_mqm_lamp_state_event(executor, event->payload.lamp_state);
        break;
    default:
        break;
    }

    g_free(event->payload.lamp_state);
    g_free(event);

    return TRUE;
}

static void update_lamp_state(SldExecutor *executor, const gchar *state)
{
    gboolean lamp_state = false;

    g_assert(executor);
    g_assert(state);

    if (g_strcmp0(state, SLD_LAMP_STATE_ON) != 0)
        lamp_state = FALSE;
    else
        lamp_state = TRUE;

    sld_gpioctl_set_led_value(executor->gpioctl, lamp_state);
}

static void do_update_mqm_lamp_state_event(SldExecutor *executor, const gchar *state)
{
    g_assert(executor);
    g_assert(state);

    update_lamp_state(executor, state);
}

static void executor_source_destroy_notify(gpointer _executor)
{
    SldExecutor *executor = (SldExecutor *) _executor;

    g_assert(executor);
    g_debug("Executor destroy notification");

    sld_executor_unref(executor);
}

static void executor_queue_destroy_notify(gpointer _executor)
{
    SLD_UNUSED(_executor);
    g_debug("Executor queue destroy notification");
}
#ifdef WITH_MQM
void btnPressed(int state, gpointer executor )
{
    SldExecutor *sdlExecutor = (SldExecutor *)executor;

    if (executor != NULL && sdlExecutor->mqmproxy != NULL)
    {
        g_info("btnPressed. Sending MQM");
        sld_mqmproxy_set_lamp_state(sdlExecutor->mqmproxy, state == 1 ? "ON" : "OFF");
    }
}
#endif
SldExecutor *sld_executor_new(SldOptions *options, SldGpioCtl *gpioctl)
{
    SldExecutor *executor
        = (SldExecutor *) g_source_new(&executor_source_funcs, sizeof(SldExecutor));

    g_assert(executor);
    g_assert(options);
    g_assert(gpioctl);

    g_ref_count_init(&executor->rc);
    executor->callback = executor_source_callback;
    executor->options = sld_options_ref(options);
    executor->gpioctl = sld_gpioctl_ref(gpioctl);
    #ifdef WITH_MQM
    executor->gpioctl->executor = executor;
    executor->gpioctl->ButtonPressed = &btnPressed;
    #endif
    executor->queue = g_async_queue_new_full(executor_queue_destroy_notify);

    g_source_set_callback(
        SLD_EVENT_SOURCE(executor), NULL, executor, executor_source_destroy_notify);
    g_source_attach(SLD_EVENT_SOURCE(executor), NULL);

    return executor;
}

SldExecutor *sld_executor_ref(SldExecutor *executor)
{
    g_assert(executor);
    g_ref_count_inc(&executor->rc);
    return executor;
}

void sld_executor_unref(SldExecutor *executor)
{
    g_assert(executor);

    if (g_ref_count_dec(&executor->rc) == TRUE) {
        if (executor->options != NULL)
            sld_options_unref(executor->options);

        if (executor->gpioctl != NULL)
            sld_gpioctl_unref(executor->gpioctl);

#ifdef WITH_MQM
        if (executor->mqmproxy != NULL)
            sld_mqmproxy_unref(executor->mqmproxy);
#endif

        g_async_queue_unref(executor->queue);
        g_source_unref(SLD_EVENT_SOURCE(executor));
    }
}

void sld_executor_push_event(SldExecutor *executor,
                             ExecutorEventType type,
                             ExecutorActionPayload payload)
{
    post_executor_event(executor, type, payload);
}

#ifdef WITH_MQM
void sld_executor_set_mqm_proxy(SldExecutor *executor, gpointer mqmproxy)
{
    g_assert(executor);
    g_assert(mqmproxy);

    executor->mqmproxy = sld_mqmproxy_ref((SldMqmProxy *) mqmproxy);
}
#endif
