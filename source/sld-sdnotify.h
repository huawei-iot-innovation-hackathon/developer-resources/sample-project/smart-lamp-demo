/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-sdnotify.h
 */

#pragma once

#include "sld-types.h"

#include <glib.h>

G_BEGIN_DECLS

/**
 * @brief The SldSDNotify opaque data structure
 */
typedef struct _SldSDNotify {
    GSource *source; /**< Event loop source */
    grefcount rc;    /**< Reference counter variable  */
} SldSDNotify;

/*
 * @brief Create a new sdnotify object
 * @return On success return a new SldSDNotify object
 */
SldSDNotify *sld_sdnotify_new(void);

/**
 * @brief Aquire sdnotify object
 * @param sdnotify Pointer to the sdnotify object
 * @return The referenced sdnotify object
 */
SldSDNotify *sld_sdnotify_ref(SldSDNotify *sdnotify);

/**
 * @brief Release sdnotify object
 * @param sdnotify Pointer to the sdnotify object
 */
void sld_sdnotify_unref(SldSDNotify *sdnotify);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(SldSDNotify, sld_sdnotify_unref);

G_END_DECLS
