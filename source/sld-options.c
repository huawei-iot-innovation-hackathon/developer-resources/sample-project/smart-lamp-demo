/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-options.c
 */

#include "sld-options.h"
#include "sld-defaults.h"

#include <glib.h>
#include <stdlib.h>

static gint64 get_long_option(SldOptions *opts,
                              const gchar *section_name,
                              const gchar *property_name,
                              GError **error);

SldOptions *sld_options_new(const gchar *conf_path)
{
    SldOptions *opts = g_new0(SldOptions, 1);

    opts->has_conf = FALSE;

    if (conf_path != NULL) {
        g_autoptr(GError) error = NULL;

        opts->conf = g_key_file_new();

        g_assert(opts->conf);

        if (g_key_file_load_from_file(opts->conf, conf_path, G_KEY_FILE_NONE, &error) == TRUE)
            opts->has_conf = TRUE;
        else
            g_debug("Cannot parse configuration file");
    }

    g_ref_count_init(&opts->rc);

    return opts;
}

SldOptions *sld_options_ref(SldOptions *opts)
{
    g_assert(opts);
    g_ref_count_inc(&opts->rc);
    return opts;
}

void sld_options_unref(SldOptions *opts)
{
    g_assert(opts);

    if (g_ref_count_dec(&opts->rc) == TRUE) {
        if (opts->conf)
            g_key_file_unref(opts->conf);

        g_free(opts);
    }
}

GKeyFile *sld_options_get_key_file(SldOptions *opts)
{
    g_assert(opts);
    return opts->conf;
}

gchar *sld_options_string_for(SldOptions *opts, SldOptionsKey key)
{
    switch (key) {
    case KEY_GPIO_CHIP_NAME:
        if (opts->has_conf) {
            char *tmp = g_key_file_get_string(opts->conf, "common", "GPIOChipName", NULL);

            if (tmp != NULL)
                return tmp;
        }
        return g_strdup(SLD_GPIO_CHIP_NAME);

    default:
        break;
    }

    g_error("No default value provided for string key");

    return NULL;
}

static gint64 get_long_option(SldOptions *opts,
                              const gchar *section_name,
                              const gchar *property_name,
                              GError **error)
{
    g_assert(opts);
    g_assert(section_name);
    g_assert(property_name);

    if (opts->has_conf) {
        g_autofree gchar *tmp
            = g_key_file_get_string(opts->conf, section_name, property_name, NULL);

        if (tmp != NULL) {
            gchar *c = NULL;
            gint64 ret;

            ret = g_ascii_strtoll(tmp, &c, 10);

            if (*c != tmp[0])
                return ret;
        }
    }

    g_set_error_literal(
        error, g_quark_from_string("sld-options"), 0, "Cannot convert option to long");

    return -1;
}

gint64 sld_options_long_for(SldOptions *opts, SldOptionsKey key)
{
    g_autoptr(GError) error = NULL;
    gint64 value = 0;

    switch (key) {
    case KEY_GPIO_CHIP_LINE_LED:
        value = get_long_option(opts, "common", "GPIOChipLineLed", &error);
        if (error != NULL)
            value = SLD_GPIO_CHIP_LINE_LED;
        break;
    case KEY_GPIO_CHIP_LINE_BTN:
        value = get_long_option(opts, "common", "GPIOChipLineBtn", &error);
        if (error != NULL)
            value = SLD_GPIO_CHIP_LINE_BTN;
        break;
    default:
        break;
    }

    return value;
}
