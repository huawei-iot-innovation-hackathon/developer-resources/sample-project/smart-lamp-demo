/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-mqmproxy.h
 */

#pragma once

#include "sld-executor.h"
#include "sld-types.h"

#include <gio/gio.h>
#include <glib.h>

G_BEGIN_DECLS

/**
 * @enum mqmproxy_event_data
 */
typedef enum _MqmProxyEventType { MQMPROXY_EVENT_BUILD_PROXY } MqmProxyEventType;

typedef gboolean (*SldMqmProxyCallback)(gpointer _mqmproxy, gpointer _event);

typedef void (*SldMqmProxyAvailableCb)(gpointer _dbus_proxy, gpointer _data);

/**
 * @struct SldMqmProxyEvent
 */
typedef struct _SldMqmProxyEvent {
    MqmProxyEventType type; /**< The event type the element holds */
} SldMqmProxyEvent;

/**
 * @struct SldMqmProxyNotifyProxy
 */
typedef struct _SldMqmProxyNotifyProxy {
    SldMqmProxyAvailableCb callback;
    gpointer data; /**< Data object to pass with the callback */
} SldMqmProxyNotifyProxy;

/**
 * @struct SldMqmProxy
 */
typedef struct _SldMqmProxy {
    GSource source;               /**< Event loop source */
    GAsyncQueue *queue;           /**< Async queue */
    SldMqmProxyCallback callback; /**< Callback function */
    SldExecutor *executor;
    grefcount rc;
    GList *notify_proxy;
    GDBusProxy *proxy;
    GDBusConnection *con;
} SldMqmProxy;

/*
 * @brief Create a new mqmproxy object
 * @return On success return a new SldMqmProxy object otherwise return NULL
 */
SldMqmProxy *sld_mqmproxy_new(SldExecutor *executor);

/**
 * @brief Aquire mqmproxy object
 */
SldMqmProxy *sld_mqmproxy_ref(SldMqmProxy *mqmproxy);

/**
 * @brief Release mqmproxy object
 */
void sld_mqmproxy_unref(SldMqmProxy *mqmproxy);

/**
 * @brief Build DBus proxy
 */
void sld_mqmproxy_build_proxy(SldMqmProxy *mqmproxy);

/**
 * @brief Get a reference to manager proxy
 */
GDBusProxy *sld_mqmproxy_get_manager_proxy(SldMqmProxy *mqmproxy);

/**
 * @brief Get existing services
 */
void sld_mqmproxy_register_proxy_cb(SldMqmProxy *mqmproxy,
                                    SldMqmProxyAvailableCb cb,
                                    gpointer data);

/**
 * @brief Set LampState in Mqm
 */
void sld_mqmproxy_set_lamp_state(SldMqmProxy *mqmproxy, const gchar *state);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(SldMqmProxy, sld_mqmproxy_unref);

G_END_DECLS
