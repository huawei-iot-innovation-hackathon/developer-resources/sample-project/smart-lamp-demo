/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-options.h
 */

#pragma once

#include "sld-types.h"

#include <glib.h>

G_BEGIN_DECLS

/**
 * @enum Option keys
 */
typedef enum _SldOptionsKey { KEY_GPIO_CHIP_NAME, KEY_GPIO_CHIP_LINE_LED, KEY_GPIO_CHIP_LINE_BTN } SldOptionsKey;

/**
 * @struct Option object
 */
typedef struct _SldOptions {
    GKeyFile *conf;    /**< The GKeyFile object */
    gboolean has_conf; /**< True if a runtime option object is available */
    grefcount rc;      /**< Reference counter variable  */
} SldOptions;

/*
 * @brief Create a new options object
 */
SldOptions *sld_options_new(const gchar *conf_path);

/*
 * @brief Aquire options object
 */
SldOptions *sld_options_ref(SldOptions *opts);

/**
 * @brief Release an options object
 */
void sld_options_unref(SldOptions *opts);

/**
 * @brief Get the GKeyFile object
 */
GKeyFile *sld_options_get_key_file(SldOptions *opts);

/*
 * @brief Get a configuration value string for key
 */
gchar *sld_options_string_for(SldOptions *opts, SldOptionsKey key);

/*
 * @brief Get a configuration gint64 value for key
 */
gint64 sld_options_long_for(SldOptions *opts, SldOptionsKey key);

G_END_DECLS
