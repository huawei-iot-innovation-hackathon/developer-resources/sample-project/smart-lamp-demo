/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-main.h
 */

#include "sld-application.h"
#include "sld-logging.h"

#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

static GMainLoop *g_mainloop = NULL;

static void terminate(int signum)
{
    g_info("Smart Lamp Demo terminate with signal %d", signum);

    if (g_mainloop != NULL)
        g_main_loop_quit(g_mainloop);
}

gint main(gint argc, gchar *argv[])
{
    g_autoptr(GOptionContext) context = NULL;
    g_autoptr(GError) error = NULL;
    g_autoptr(SldApplication) app = NULL;
    g_autofree gchar *config_path = NULL;
    gboolean version = FALSE;
    SldStatus status = SLD_STATUS_OK;

    GOptionEntry main_entries[] = {
        {"version", 'v', 0, G_OPTION_ARG_NONE, &version, "Show program version", ""},
        {"config", 'c', 0, G_OPTION_ARG_FILENAME, &config_path, "Override configuration file", ""},
        {0}};

    signal(SIGINT, terminate);
    signal(SIGTERM, terminate);

    context = g_option_context_new("- Smart Lamp service daemon");
    g_option_context_set_summary(context, "The service is managing lamp state");
    g_option_context_add_main_entries(context, main_entries, NULL);

    if (!g_option_context_parse(context, &argc, &argv, &error)) {
        g_printerr("%s\n", error->message);
        return EXIT_FAILURE;
    }

    if (version) {
        g_printerr("%s\n", SLD_VERSION);
        return EXIT_SUCCESS;
    }

    sld_logging_open("SLD", "Smart Lamp Demo Service", "SLD", "Default context");

    if (config_path == NULL)
        config_path = g_build_filename(SLD_CONFIG_DIRECTORY, SLD_CONFIG_FILE_NAME, NULL);

    if (g_access(config_path, R_OK) == 0) {
        app = sld_application_new(config_path, &error);
        if (error != NULL) {
            g_printerr("%s\n", error->message);
            status = SLD_STATUS_ERROR;
        } else {
            g_info("Smart Lamp Demo service started");
            g_mainloop = sld_application_get_mainloop(app);
            status = sld_application_execute(app);
        }
    } else
        g_warning("Cannot open configuration file %s", config_path);

    sld_logging_close();

    return status == SLD_STATUS_OK ? EXIT_SUCCESS : EXIT_FAILURE;
}
