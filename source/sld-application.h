/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-application.h
 */

#pragma once

#include "sld-defaults.h"
#include "sld-options.h"
#include "sld-types.h"
#ifdef WITH_SYSTEMD
#include "sld-sdnotify.h"
#endif
#include "sld-executor.h"
#include "sld-gpioctl.h"
#ifdef WITH_MQM
#include "sld-mqmproxy.h"
#endif

#include <glib.h>
#include <stdlib.h>

G_BEGIN_DECLS

/**
 * @brief SldApplication application object referencing main objects
 */
typedef struct _SldApplication {
    SldOptions *options;
#ifdef WITH_SYSTEMD
    SldSDNotify *sdnotify;
#endif
    SldGpioCtl *gpioctl;
    SldExecutor *executor;
#ifdef WITH_MQM
    SldMqmProxy *mqmproxy;
#endif
    GMainLoop *mainloop;
    grefcount rc;
} SldApplication;

/**
 * @brief Create a new SldApplication object
 * @param config Full path to the configuration file
 * @param error An error object must be provided. If an error occurs during
 * initialization the error is reported and application should not use the
 * returned object. If the error is set the object is invalid and needs to be
 * released.
 */
SldApplication *sld_application_new(const gchar *config, GError **error);

/**
 * @brief Aquire SldApplication object
 * @param app The object to aquire
 * @return The aquiered SldApplication object
 */
SldApplication *sld_application_ref(SldApplication *app);

/**
 * @brief Release a SldApplication object
 * @param app The sld application object to release
 */
void sld_application_unref(SldApplication *app);

/**
 * @brief Execute sld application
 * @param app The sld application object
 * @return If run was succesful SLD_STATUS_OK is returned
 */
SldStatus sld_application_execute(SldApplication *app);

/**
 * @brief Get main event loop reference
 * @param app The sld application object
 * @return A pointer to the main event loop
 */
GMainLoop *sld_application_get_mainloop(SldApplication *app);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(SldApplication, sld_application_unref);

G_END_DECLS
