/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-application.c
 */

#include "sld-application.h"

#include <errno.h>
#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

SldApplication *sld_application_new(const gchar *config, GError **error)
{
    SldApplication *app = g_new0(SldApplication, 1);

    g_assert(app);
    g_assert(error);

    g_ref_count_init(&app->rc);
#ifdef WITH_SYSTEMD
    /* construct sdnotify */
    app->sdnotify = sld_sdnotify_new();
#endif
    /* construct options */
    app->options = sld_options_new(config);
    /* construct gpioctl */
    app->gpioctl = sld_gpioctl_new(app->options);
    /* construct executor */
    app->executor = sld_executor_new(app->options, app->gpioctl);
#ifdef WITH_MQM
    /* construct mqmproxy */
    app->mqmproxy = sld_mqmproxy_new(app->executor);
    sld_executor_set_mqm_proxy(app->executor, app->mqmproxy);
    sld_mqmproxy_build_proxy(app->mqmproxy);
#endif
    /* construct main loop */
    app->mainloop = g_main_loop_new(NULL, TRUE);

    return app;
}

SldApplication *sld_application_ref(SldApplication *app)
{
    g_assert(app);
    g_ref_count_inc(&app->rc);
    return app;
}

void sld_application_unref(SldApplication *app)
{
    g_assert(app);

    if (g_ref_count_dec(&app->rc) == TRUE) {
#ifdef WITH_SYSTEMD
        if (app->sdnotify != NULL)
            sld_sdnotify_unref(app->sdnotify);
#endif
        if (app->options != NULL)
            sld_options_unref(app->options);

        if (app->gpioctl != NULL)
            sld_gpioctl_unref(app->gpioctl);

        if (app->executor != NULL)
            sld_executor_unref(app->executor);

#ifdef WITH_MQM
        if (app->mqmproxy != NULL)
            sld_mqmproxy_unref(app->mqmproxy);
#endif

        if (app->mainloop != NULL)
            g_main_loop_unref(app->mainloop);

        g_free(app);
    }
}

GMainLoop *sld_application_get_mainloop(SldApplication *app)
{
    g_assert(app);
    return app->mainloop;
}

SldStatus sld_application_execute(SldApplication *app)
{
    /* run the main event loop */
    g_main_loop_run(app->mainloop);

    return SLD_STATUS_OK;
}
