/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-gpioctl.h
 */

#pragma once

#include "sld-options.h"
#include "sld-types.h"

#include <glib.h>
#include <gpiod.h>

G_BEGIN_DECLS

/**
 * @struct Les state enum
 */
typedef enum _LedState
{
    LedStateON,
    LedStateOFF,
    LedStateUnknown,

}LedState;
/**
 * @struct Option object
 */
typedef struct _SldGpioCtl {
    struct gpiod_chip *chip;
    struct gpiod_line *line_led;
    struct gpiod_line *line_btn;
    gchar *chip_name;
    glong chip_line_led;
    glong chip_line_btn;
    gpointer executor;
    void (*ButtonPressed)(int state, gpointer executor);
    LedState led_state;
    GSource *source; /**< Event loop source */
    grefcount rc; /**< Reference counter variable  */
} SldGpioCtl;


typedef enum _ButtonState
{
    ButtonStateON,
    ButtonStateOFF,
}ButtonState;
/*
 * @brief Create a new gpioctl object
 */
SldGpioCtl *sld_gpioctl_new(SldOptions *options);

/*
 * @brief Aquire gpioctl object
 */
SldGpioCtl *sld_gpioctl_ref(SldGpioCtl *gpioctl);

/**
 * @brief Release gpioctl object
 */
void sld_gpioctl_unref(SldGpioCtl *gpioctl);

/**
 * @brief Set GPIO value for lamp control
 */
void sld_gpioctl_set_led_value(SldGpioCtl *gpioctl, gint value);

/**
 * @brief Returns information is the button pressed.
 */
ButtonState sld_gpioctl_get_btn_value(SldGpioCtl *gpioctl);
G_END_DECLS
