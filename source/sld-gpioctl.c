/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-gpioctl.c
 */

#include "sld-gpioctl.h"

#include <glib.h>

static gboolean source_timer_callback(gpointer data)
{

    int pressed = 0;
    static int prevVal = 0;
    SldGpioCtl* chip = (SldGpioCtl*)data;
    int curVal = sld_gpioctl_get_btn_value(chip);
    pressed = (prevVal == 0 && curVal == 1);
    prevVal = curVal;

    if(pressed)
    {

        if (chip->led_state == LedStateON)
        {
            g_info("Turning off");
            sld_gpioctl_set_led_value(chip, FALSE);
            if(chip->ButtonPressed != NULL)
                chip->ButtonPressed(0, chip->executor);
        }
        else
        {
            g_info("Turning on");
            sld_gpioctl_set_led_value(chip, TRUE);
            if(chip->ButtonPressed != NULL)
                chip->ButtonPressed(1, chip->executor);
        }
    }

    return TRUE;
}
static void source_destroy_notify(gpointer data)
{
    SLD_UNUSED(data);
    g_info("System watchdog disabled");
}
SldGpioCtl *sld_gpioctl_new(SldOptions *options)
{
    SldGpioCtl *gpioctl = g_new0(SldGpioCtl, 1);

    int ret = -1;

    g_assert(gpioctl);
    g_ref_count_init(&gpioctl->rc);

    gpioctl->chip_name = sld_options_string_for(options, KEY_GPIO_CHIP_NAME);
    gpioctl->chip_line_led = sld_options_long_for(options, KEY_GPIO_CHIP_LINE_LED);

    gpioctl->chip = gpiod_chip_open_by_name(gpioctl->chip_name);
    if (gpioctl->chip == NULL)
        g_error("Open chip failed");

    gpioctl->line_led = gpiod_chip_get_line(gpioctl->chip, gpioctl->chip_line_led);
    if (gpioctl->line_led == NULL)
        g_error("Open line failed for led");

    ret = gpiod_line_request_output(gpioctl->line_led, "LAMP", 0);
    if (ret < 0)
        g_error("Request line as output failed");


    gpioctl->chip_line_btn = sld_options_long_for(options, KEY_GPIO_CHIP_LINE_BTN);
    gpioctl->line_btn = gpiod_chip_get_line(gpioctl->chip, gpioctl->chip_line_btn);
    if (gpioctl->line_btn == NULL)
        g_error("Open line failed for button");

    ret = gpiod_line_request_input(gpioctl->line_btn, "BTN");
    if (ret < 0)
        g_error("Request btn as input failed");

    // 100 ms timer to check button press.
    gpioctl->source = g_timeout_source_new(100);
    g_source_ref(gpioctl->source);

    g_source_set_callback(gpioctl->source,
                            G_SOURCE_FUNC(source_timer_callback),
                            gpioctl,
                            source_destroy_notify);
    g_source_attach(gpioctl->source, NULL);

    return gpioctl;
}

SldGpioCtl *sld_gpioctl_ref(SldGpioCtl *gpioctl)
{
    g_assert(gpioctl);
    g_ref_count_inc(&gpioctl->rc);
    return gpioctl;
}

void sld_gpioctl_unref(SldGpioCtl *gpioctl)
{
    g_assert(gpioctl);

    if (g_ref_count_dec(&gpioctl->rc) == TRUE) {
        if (gpioctl->line_led != NULL)
            gpiod_line_release(gpioctl->line_led);
        if (gpioctl->line_btn != NULL)
            gpiod_line_release(gpioctl->line_btn);
        if (gpioctl->chip != NULL)
            gpiod_chip_close(gpioctl->chip);
        if (gpioctl->source != NULL)
            g_source_unref(gpioctl->source);

        g_free(gpioctl->chip_name);
        g_free(gpioctl);
    }
}

void sld_gpioctl_set_led_value(SldGpioCtl *gpioctl, gint value)
{
    int ret = -1;

    g_assert(gpioctl);
    g_assert(gpioctl->line_led);

    ret = gpiod_line_set_value(gpioctl->line_led, ((value != 0) ? 1 : 0));
    if (ret < 0)
    {
        g_warning("Set line output failed");
        gpioctl->led_state = LedStateUnknown;
    }
    else
    {
        gpioctl->led_state = (value != 0) ? LedStateON : LedStateOFF;
    }

}

ButtonState sld_gpioctl_get_btn_value(SldGpioCtl *gpioctl)
{
    int ret = -1;

    g_assert(gpioctl);
    g_assert(gpioctl->line_btn);

    ret = gpiod_line_get_value(gpioctl->line_btn);
    if (ret < 0)
        g_warning("Get line input failed");

    // PIN 5 has internal PULL UP. So when it is not pressed it has value 1
    // Pressing button sets PIN5 ground and ret is 0.
    if(ret == 0)
    {
        return ButtonStateON;
    }
    return ButtonStateOFF;

}