/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-executor.h
 */

#pragma once

#include "sld-gpioctl.h"
#include "sld-options.h"
#include "sld-types.h"

#include <gio/gio.h>
#include <glib.h>

G_BEGIN_DECLS

/**
 * @enum executor_event_data
 * @brief Executor event data type
 */
typedef enum _ExecutorEventType {
    EXECUTOR_UPDATE_MQM_LAMP_STATE
} ExecutorEventType;

/**
 * @function SldExecutorCallback
 * @brief Custom callback used internally by SldExecutor as source callback
 */
typedef gboolean (*SldExecutorCallback)(gpointer _executor, gpointer _event);

/**
 * @struct ExecutorActionPayload
 * @brief The Action Event Payload
 */
typedef struct _ExecutorActionPayload {
    gchar *lamp_state; /**< The lamp state */
} ExecutorActionPayload;

/**
 * @struct SldExecutorEvent
 * @brief The Executor Event
 */
typedef struct _SldExecutorEvent {
    ExecutorEventType type;        /**< The event type the element holds */
    ExecutorActionPayload payload; /**< The event payload the element holds */
} SldExecutorEvent;

/**
 * @struct SldExecutor
 * @brief The SldExecutor opaque data structure
 */
typedef struct _SldExecutor {
    GSource source; /**< Event loop source */
    SldOptions *options;
    SldGpioCtl *gpioctl;
    GAsyncQueue *queue;           /**< Async queue */
    SldExecutorCallback callback; /**< Callback function */

#ifdef WITH_MQM
    gpointer mqmproxy;
#endif
    grefcount rc;
} SldExecutor;

/*
 * @brief Create a new executor object
 * @return On success return a new SldExecutor object otherwise return NULL
 */
SldExecutor *sld_executor_new(SldOptions *options, SldGpioCtl *gpioctl);

/**
 * @brief Aquire executor object
 */
SldExecutor *sld_executor_ref(SldExecutor *executor);

/**
 * @brief Release executor object
 */
void sld_executor_unref(SldExecutor *executor);

/**
 * @brief Push an executor event
 */
void sld_executor_push_event(SldExecutor *executor,
                             ExecutorEventType type,
                             ExecutorActionPayload payload);

#ifdef WITH_MQM
/**
 * @brief Set MqmProxy object
 */
void sld_executor_set_mqm_proxy(SldExecutor *executor, gpointer mqmproxy);
#endif

G_DEFINE_AUTOPTR_CLEANUP_FUNC(SldExecutor, sld_executor_unref);

G_END_DECLS
