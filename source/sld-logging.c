/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-logging.c
 */

#include "sld-logging.h"
#include "sld-types.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

static void sld_logging_handler(const gchar *log_domain,
                                GLogLevelFlags log_level,
                                const gchar *message,
                                gpointer user_data);

static int priority_to_syslog(int priority)
{
    switch (priority) {
    case G_LOG_FLAG_RECURSION:
    case G_LOG_FLAG_FATAL:
        return LOG_CRIT;
    case G_LOG_LEVEL_ERROR:
    case G_LOG_LEVEL_CRITICAL:
        return LOG_ERR;
    case G_LOG_LEVEL_WARNING:
        return LOG_WARNING;
    case G_LOG_LEVEL_MESSAGE:
    case G_LOG_LEVEL_INFO:
        return LOG_INFO;
    case G_LOG_LEVEL_DEBUG:
    default:
        return LOG_DEBUG;
    }
}

void sld_logging_open(const gchar *app_name,
                      const gchar *app_desc,
                      const gchar *ctx_name,
                      const gchar *ctx_desc)
{
    SLD_UNUSED(app_name);
    SLD_UNUSED(app_desc);
    SLD_UNUSED(ctx_name);
    SLD_UNUSED(ctx_desc);

    g_log_set_default_handler(sld_logging_handler, NULL);
}

static void sld_logging_handler(const gchar *log_domain,
                                GLogLevelFlags log_level,
                                const gchar *message,
                                gpointer user_data)
{
    SLD_UNUSED(log_domain);
    SLD_UNUSED(user_data);

    syslog(priority_to_syslog(log_level), "%s", message);
}

void sld_logging_close(void) { }
