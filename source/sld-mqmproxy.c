/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-mqmproxy.c
 */

#include "sld-mqmproxy.h"

const gchar *sld_mqm_dbus_name = "com.huawei.mqttmanager";
const gchar *sld_mqm_dbus_object_path = "/com/huawei/mqttmanager";
const gchar *sld_mqm_dbus_interface_lamp = "com.huawei.mqttmanager.Lamp";

/**
 * @brief Post new event
 * @param mqmproxy A pointer to the mqmproxy object
 * @param type The type of the new event to be posted
 */
static void post_mqmproxy_event(SldMqmProxy *mqmproxy, MqmProxyEventType type);
/**
 * @brief GSource prepare function
 */
static gboolean mqmproxy_source_prepare(GSource *source, gint *timeout);
/**
 * @brief GSource dispatch function
 */
static gboolean mqmproxy_source_dispatch(GSource *source, GSourceFunc callback, gpointer _mqmproxy);
/**
 * @brief GSource callback function
 */
static gboolean mqmproxy_source_callback(gpointer _mqmproxy, gpointer _event);
/**
 * @brief GSource destroy notification callback function
 */
static void mqmproxy_source_destroy_notify(gpointer _mqmproxy);
/**
 * @brief Async queue destroy notification callback function
 */
static void mqmproxy_queue_destroy_notify(gpointer _mqmproxy);
/**
 * @brief Build proxy local
 */
static void mqmproxy_build_proxy(SldMqmProxy *mqmproxy);
/**
 * @brief Callback for a notify proxy entry
 */
static void call_notify_proxy_entry(gpointer _notify_object, gpointer _dbus_proxy);

/**
 * @brief GSourceFuncs vtable
 */
static GSourceFuncs mqmproxy_source_funcs = {
    mqmproxy_source_prepare,
    NULL,
    mqmproxy_source_dispatch,
    NULL,
    NULL,
    NULL,
};

static void post_mqmproxy_event(SldMqmProxy *mqmproxy, MqmProxyEventType type)
{
    SldMqmProxyEvent *e = NULL;

    g_assert(mqmproxy);

    e = g_new0(SldMqmProxyEvent, 1);
    e->type = type;

    g_async_queue_push(mqmproxy->queue, e);
}

static gboolean mqmproxy_source_prepare(GSource *source, gint *timeout)
{
    SldMqmProxy *mqmproxy = (SldMqmProxy *) source;

    SLD_UNUSED(timeout);

    return (g_async_queue_length(mqmproxy->queue) > 0);
}

static gboolean mqmproxy_source_dispatch(GSource *source, GSourceFunc callback, gpointer _mqmproxy)
{
    SldMqmProxy *mqmproxy = (SldMqmProxy *) source;
    gpointer event = g_async_queue_try_pop(mqmproxy->queue);

    SLD_UNUSED(callback);
    SLD_UNUSED(_mqmproxy);

    if (event == NULL)
        return G_SOURCE_CONTINUE;

    return mqmproxy->callback(mqmproxy, event) == TRUE ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
}

static gboolean mqmproxy_source_callback(gpointer _mqmproxy, gpointer _event)
{
    SldMqmProxy *mqmproxy = (SldMqmProxy *) _mqmproxy;
    SldMqmProxyEvent *event = (SldMqmProxyEvent *) _event;

    g_assert(mqmproxy);
    g_assert(event);

    switch (event->type) {
    case MQMPROXY_EVENT_BUILD_PROXY:
        mqmproxy_build_proxy(mqmproxy);
        break;
    default:
        break;
    }

    g_free(event);

    return TRUE;
}

static void mqmproxy_source_destroy_notify(gpointer _mqmproxy)
{
    SldMqmProxy *mqmproxy = (SldMqmProxy *) _mqmproxy;

    g_assert(mqmproxy);
    g_debug("MqmProxy destroy notification");

    sld_mqmproxy_unref(mqmproxy);
}

static void mqmproxy_queue_destroy_notify(gpointer _mqmproxy)
{
    SLD_UNUSED(_mqmproxy);
    g_debug("MqmProxy queue destroy notification");
}

static void on_manager_signal(GDBusProxy *proxy,
                              gchar *sender_name,
                              gchar *signal_name,
                              GVariant *parameters,
                              gpointer user_data)
{
    SldMqmProxy *mqmproxy = (SldMqmProxy *) user_data;

    SLD_UNUSED(proxy);
    SLD_UNUSED(sender_name);

    if (g_strcmp0(signal_name, "NewLampState") == 0) {
        const gchar *new_lamp_state = NULL;

        g_variant_get(parameters, "(s)", &new_lamp_state);

        if (new_lamp_state != NULL) {
            ExecutorActionPayload pld = {.lamp_state = g_strdup(new_lamp_state)};

            g_debug("Dispatch new lamp state '%s'", new_lamp_state);
            sld_executor_push_event(mqmproxy->executor, EXECUTOR_UPDATE_MQM_LAMP_STATE, pld);
        } else
            g_warning("Fail to read date on NewLampState signal");
    }
}

static void call_notify_proxy_entry(gpointer _notify_object, gpointer _dbus_proxy)

{
    SldMqmProxyNotifyProxy *notify_object = (SldMqmProxyNotifyProxy *) _notify_object;
    GDBusProxy *proxy = (GDBusProxy *) _dbus_proxy;

    g_assert(notify_object);
    g_assert(proxy);

    if (notify_object->callback != NULL)
        notify_object->callback(proxy, notify_object->data);

    /* we don't free the notify object since will be freed with mqmproxy */
}

static void mqmproxy_build_proxy(SldMqmProxy *mqmproxy)
{
    g_autoptr(GError) error = NULL;

    g_assert(mqmproxy);

    mqmproxy->con = g_bus_get_sync(G_BUS_TYPE_SYSTEM, NULL, &error);
    if (error) {
        g_error("Failed to get bus access - %i : %s\n", error->code, error->message);
        g_error_free(error);
    }

    mqmproxy->proxy = g_dbus_proxy_new_for_bus_sync(G_BUS_TYPE_SYSTEM,
                                                    G_DBUS_PROXY_FLAGS_NONE,
                                                    NULL, /* GDBusInterfaceInfo */
                                                    sld_mqm_dbus_name,
                                                    sld_mqm_dbus_object_path,
                                                    sld_mqm_dbus_interface_lamp,
                                                    NULL, /* GCancellable */
                                                    &error);

    if (error != NULL)
        g_error("Fail to build proxy for Mqm. Error %s", error->message);
    else {
        g_signal_connect(mqmproxy->proxy, "g-signal", G_CALLBACK(on_manager_signal), mqmproxy);

        g_list_foreach(mqmproxy->notify_proxy, call_notify_proxy_entry, mqmproxy->proxy);
    }
}

GDBusProxy *sld_mqmproxy_get_manager_proxy(SldMqmProxy *mqmproxy)
{
    g_assert(mqmproxy);
    return mqmproxy->proxy;
}

static void remove_notify_proxy_entry(gpointer _entry)
{
    g_free(_entry);
}

SldMqmProxy *sld_mqmproxy_new(SldExecutor *executor)
{
    SldMqmProxy *mqmproxy
        = (SldMqmProxy *) g_source_new(&mqmproxy_source_funcs, sizeof(SldMqmProxy));

    g_assert(mqmproxy);
    g_assert(executor);

    g_ref_count_init(&mqmproxy->rc);
    mqmproxy->executor = sld_executor_ref(executor);
    mqmproxy->queue = g_async_queue_new_full(mqmproxy_queue_destroy_notify);
    mqmproxy->callback = mqmproxy_source_callback;

    g_source_set_callback(
        SLD_EVENT_SOURCE(mqmproxy), NULL, mqmproxy, mqmproxy_source_destroy_notify);
    g_source_attach(SLD_EVENT_SOURCE(mqmproxy), NULL);

    return mqmproxy;
}

SldMqmProxy *sld_mqmproxy_ref(SldMqmProxy *mqmproxy)
{
    g_assert(mqmproxy);
    g_ref_count_inc(&mqmproxy->rc);
    return mqmproxy;
}

void sld_mqmproxy_unref(SldMqmProxy *mqmproxy)
{
    g_assert(mqmproxy);

    if (g_ref_count_dec(&mqmproxy->rc) == TRUE) {
        if (mqmproxy->executor != NULL)
            sld_executor_unref(mqmproxy->executor);

        if (mqmproxy->proxy != NULL)
            g_object_unref(mqmproxy->proxy);

        g_async_queue_unref(mqmproxy->queue);
        g_list_free_full(mqmproxy->notify_proxy, remove_notify_proxy_entry);
        g_source_unref(SLD_EVENT_SOURCE(mqmproxy));
    }
}

void sld_mqmproxy_build_proxy(SldMqmProxy *mqmproxy)
{
    post_mqmproxy_event(mqmproxy, MQMPROXY_EVENT_BUILD_PROXY);
}

void sld_mqmproxy_register_proxy_available_callback(SldMqmProxy *mqmproxy,
                                                    SldMqmProxyAvailableCb callback,
                                                    gpointer data)
{
    SldMqmProxyNotifyProxy *notify = NULL;

    g_assert(mqmproxy);
    g_assert(callback);
    g_assert(data);

    notify = g_new0(SldMqmProxyNotifyProxy, 1);

    notify->callback = callback;
    notify->data = data;

    mqmproxy->notify_proxy = g_list_prepend(mqmproxy->notify_proxy, notify);
}

void sld_mqmproxy_set_lamp_state(SldMqmProxy *mqmproxy, const gchar *state)
{
    g_autoptr(GDBusMessage) msg = NULL;
    g_autoptr(GDBusMessage) rsp = NULL;
    g_autoptr(GError) error = NULL;

    g_assert(mqmproxy);
    g_assert(state);

    msg = g_dbus_message_new_method_call(
        sld_mqm_dbus_name, sld_mqm_dbus_object_path, sld_mqm_dbus_interface_lamp, "setLampState");
    g_dbus_message_set_body(msg, g_variant_new("(s)", state));
    rsp = g_dbus_connection_send_message_with_reply_sync(
        mqmproxy->con, msg, G_DBUS_SEND_MESSAGE_FLAGS_NONE, -1, NULL, NULL, &error);

    if (error) {
        g_warning("Failed to call remote method - %i : %s\n", error->code, error->message);
        g_error_free(error);
        error = NULL;
    }

    SLD_UNUSED(rsp);
}
