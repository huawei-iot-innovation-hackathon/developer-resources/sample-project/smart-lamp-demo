# Smart Lamp Demo
Smart Lamp Demo application that interacts with connectivity managers on DBus. Smart Lamp can switch GPIO21 (LED) On and Off.
See building instructions from:
[GettingStarted.md] (https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/sample-project/documentation/-/blob/main/GettingStarted.md)
## Description
The application registers for NewLampState signal on DBUS which is broadcasted by managers when the Smart Lamp switch state changes in connectivity clouds.
When the signal is received the configured GPIO associated with the lamp switch is set or unset depending on the new state.
The interaction with the cloud is done via the manager services:
eg. https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/resources-oniro/mqtt-manager

## Dependencies
| Component | Source |
| ------ | ------ |
| libgpiod | https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/ |
| GLib | https://wiki.gnome.org/Projects/GLib |

## Build options
| Option | Default | Description |
| ------ | ------ | ------ |
| CONFDIR | /etc | Default configuration directory |
| SYSTEMD | true | Enable systemd watchdog if run as service |
| GPIOD | true | Enable libgpiod dependecy (useful for debug) |
| MQM | true | Enable MQTT Manager support |
