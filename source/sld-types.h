/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @file sld-types.h
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

#ifndef SLD_UNUSED
#define SLD_UNUSED(x) (void) (x)
#endif

#define SLD_EVENT_SOURCE(x) (GSource *) (x)

#define SLD_LAMP_STATE_ON "ON"
#define SLD_LAMP_STATE_OFF "OFF"

typedef enum _SldStatus { SLD_STATUS_ERROR = -1, SLD_STATUS_OK } SldStatus;

G_END_DECLS
